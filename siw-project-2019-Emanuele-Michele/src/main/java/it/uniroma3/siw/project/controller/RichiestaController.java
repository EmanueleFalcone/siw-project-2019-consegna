package it.uniroma3.siw.project.controller;

import javax.validation.Valid;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import it.uniroma3.siw.project.model.Richiesta;
import it.uniroma3.siw.project.service.RichiestaService;

@Controller
public class RichiestaController {
	
	@Autowired
	RichiestaService richiestaService;
	
		@RequestMapping("/addRichiesta")
		public String addrichiesta(@Valid @RequestParam("titoloFoto") String titoloFoto, Model model) {
		model.addAttribute("richiesta", new Richiesta() );
		model.addAttribute("titoloFoto", titoloFoto );
	    return "/richiestaForm";
	    }
	
	@RequestMapping(value = "/invioRichiestaFoto", method = RequestMethod.POST)
	public String newAllievo(@Valid @ModelAttribute("richiesta") Richiesta richiesta, Model model, BindingResult bindingResult) {
	    if (this.richiestaService.esistente(richiesta)) {
	        model.addAttribute("exists", "Il richiesta già esiste");
	        return "/richiestaForm";
	    } else {
	        if (!bindingResult.hasErrors()) {
	            this.richiestaService.inserisciRichiesta(richiesta);
	            model.addAttribute("richieste", this.richiestaService.tutteRichieste());
	            return "/index";
	        }
	    }
	    return "/index";
	}
	
	@RequestMapping(value = "/richiestaAdmin", method = RequestMethod.GET)
    public String richiestaAdmin(Model model, @Valid @ModelAttribute("richiesta") Richiesta richiesta) {
		model.addAttribute("richieste", richiestaService.tutteRichieste());
		return "/richiestaAdmin";
	} 

}
