package it.uniroma3.siw.project.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import it.uniroma3.siw.project.model.Album;
import it.uniroma3.siw.project.model.Foto;
import it.uniroma3.siw.project.model.Fotografo;
import it.uniroma3.siw.project.model.Richiesta;
import it.uniroma3.siw.project.service.AlbumService;
import it.uniroma3.siw.project.service.FotoService;
import it.uniroma3.siw.project.service.FotografoService;
import it.uniroma3.siw.project.service.RichiestaService;

@Component
public class DBPopulation implements ApplicationRunner{
	
	@Autowired
	public FotografoRepository fotografoRepository;
	
	@Autowired
	public FotografoService fotografoService;
	
	@Autowired
	public AlbumService albumService;
	
	@Autowired
	public FotoService fotoService;
	
	@Autowired
	public AlbumRepository albumRepository;
	
	@Autowired
	public RichiestaService richiestaService;
	
	@Override
	public void run(ApplicationArguments args) throws Exception {
		this.deleteAll();
		this.addAll();
	}
	
	private void deleteAll() {
			
	}
	
	private void addAll() {
		Fotografo ft1 = new Fotografo("Emanuele", "Falcone", "EF");
		Fotografo ft2 = new Fotografo("Michele", "Gallegos", "MG");
		this.fotografoService.inserisciFotografo(ft1);
		this.fotografoService.inserisciFotografo(ft2);
		
		Album alb1 = new Album("PaesaggiEF", "EF", "05/12/1997");
		Album alb2 = new Album("PaesaggiMG", "MG", "29/06/1996");
		this.albumService.inserisciAlbum(alb1);
		this.albumService.inserisciAlbum(alb2);
		
		Richiesta r1 = new Richiesta("Paolo Calisse", "PaoloCalisse@gmail.com","Foto 2", "Vorrei richiedere l'utilizzo di questa foto");
		Richiesta r2 = new Richiesta("Federico Pini", "FedericoPini@gmail.com","Foto 4", "Bella foto, vorrei acquistarne il diritto di utilizzo");
		Richiesta r3 = new Richiesta("Alessio Ranieri", "AlessioRanieri@gmail.com","Foto 6", "Vorrei ricevere informazioni sui costi e le modalità di pagamento riguardanti questa foto");
		this.richiestaService.inserisciRichiesta(r1);
		this.richiestaService.inserisciRichiesta(r2);
		this.richiestaService.inserisciRichiesta(r3);
		
		Foto foto1 = new Foto("Foto 1", "PaesaggiEF","EF","https://www.turismo.it/fileadmin/mediafiles/multimedia/201705/images/670x400/Pamukkale.jpg");
		Foto foto2 = new Foto("Foto 2", "PaesaggiEF","EF","https://www.skyscanner.it/wp-content/uploads/2018/05/GettyImages-484569884.jpg?fit=1048,699");
		Foto foto3 = new Foto("Foto 3", "PaesaggiEF","EF","https://photoblog.stefanoventuri.eu/wp-content/uploads/2018/05/paesaggi.jpg");
		Foto foto4 = new Foto("Foto 4", "PaesaggiMG","MG","https://www.fotografiamoderna.it/wp-content/uploads/2017/12/Fotografie-paesaggi-consigli.jpg");
		Foto foto5 = new Foto("Foto 5", "PaesaggiMG","MG","https://photoblog.stefanoventuri.eu/wp-content/uploads/2018/03/paesaggi.jpg");
		Foto foto6 = new Foto("Foto 6", "PaesaggiMG","MG","https://www.turismo.it/typo3temp/pics/e5ddf8caf6.jpg");
		this.fotoService.inserisciFoto(foto1);
		this.fotoService.inserisciFoto(foto2);
		this.fotoService.inserisciFoto(foto3);
		this.fotoService.inserisciFoto(foto4);
		this.fotoService.inserisciFoto(foto5);
		this.fotoService.inserisciFoto(foto6);
	}

}



