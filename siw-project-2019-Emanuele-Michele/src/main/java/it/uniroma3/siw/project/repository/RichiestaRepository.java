package it.uniroma3.siw.project.repository;



import org.springframework.data.repository.CrudRepository;

import it.uniroma3.siw.project.model.Richiesta;

public interface RichiestaRepository extends CrudRepository<Richiesta, Long> {
	
	public Richiesta findByNominativo(String nominativo);
	public Richiesta findByTitoloFoto (String titoloFoto);
	
}


