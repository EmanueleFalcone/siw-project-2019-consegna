package it.uniroma3.siw.project.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.uniroma3.siw.project.model.Richiesta;
import it.uniroma3.siw.project.repository.RichiestaRepository;

@Service
public class RichiestaService {
	
	@Autowired
	public RichiestaRepository richiestaRepository;
	
	@Transactional
	public Richiesta inserisciRichiesta(Richiesta richiesta) {
		return richiestaRepository.save(richiesta);
	}
	
	@Transactional
	public List<Richiesta> tutteRichieste(){
		return (List<Richiesta>)richiestaRepository.findAll();
	}
	
	@Transactional
	public Richiesta richiestaPerNominativo(String nominativo){
		return this.richiestaRepository.findByNominativo(nominativo);
	}
	
	public boolean esistente(Richiesta richiesta) {
		Richiesta richiestaNuova = this.richiestaRepository.findByNominativo(richiesta.getNominativo());
		if (richiestaNuova!=null)
			return true;
		else 
			return false;
	}	
}
